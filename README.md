#  ChessGame


## Motivation
 
This is the frist project i had ever made. In my study i had to program with 
JavaFx and Java 8. Later i updated the code to Java 11 and did some refactoring
and used more libraries like Lombok. Vison of a project of a chessgame with
support of mutliple chessnotations an ki's is not finished. I will reuse as much
as i can for a similar spring boot project. What's not implemented are 
checkmate, castling and remi.

You can download last artifact for linux [here](https://gitlab.com/till_johanndeiter/chessgame/-/jobs/artifacts/master/raw/target/chessgame-0.0.1-SNAPSHOT.zip?job=deploy)


Due to unclear license i have to remove the ki.


## Manual

If you start chessgame with *--gui* as parameter  

chessgame --gui

you can play with a graphical interface. Otherwise you can play in a simplified
fen notation in terminal.