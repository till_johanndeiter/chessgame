package de.tjohanndeiter.external;


import de.tjohanndeiter.model.chessmen.ChessMan;
import de.tjohanndeiter.model.chessmen.Color;
import de.tjohanndeiter.exception.parse.ParseException;
import de.tjohanndeiter.exception.parse.ParseStateException;
import de.tjohanndeiter.model.GameManger;
import de.tjohanndeiter.model.Field;
import de.tjohanndeiter.model.GameState;
import de.tjohanndeiter.parser.ChessManFactory;
import de.tjohanndeiter.parser.fen.ChessManFactoryImpl;
import de.tjohanndeiter.utils.Quadruple;
import de.tjohanndeiter.utils.TArrayList;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDateTime;

public class Load {

    private static final String INFO_TAG = "<Info>";
    private static final String CURRENT_STATE_TAG = "<CurrentState>";
    private static final int BOARD_SIZE = 8;
    private static final int POS_OF_DATE_TIME = 3;
    private static final int POS_OF_BOARD = 0;
    private static final int POS_OF_CURRENT_PLAYER = 1;
    private static final int POS_OF_QUAD = 2;

    private ChessManFactory chessManFactory = new ChessManFactoryImpl();
    private GameManger chessBoard;

    public Load(final GameManger chessBoard) {
        this.chessBoard = chessBoard;
    }

    public void loadItemsToChessBoard(File file) throws IOException, ParseException {
        if (file != null) {
            String fileString = new String(Files.readAllBytes(file.toPath()));
            String[] oldStatesAndCurrent = fileString.split(CURRENT_STATE_TAG);
            String[] oldStates = oldStatesAndCurrent[0].split("<State>");
            TArrayList<GameState> backlog = new TArrayList<>();
            for (int i = 1; i < oldStates.length; i++) {
                backlog.add(parseState(oldStates[i]));
            }
            chessBoard.setBackLog(backlog);
            String[] currentState = oldStatesAndCurrent[1].split(CURRENT_STATE_TAG);
            chessBoard.setBoard(parseBoard(currentState[0].split(INFO_TAG)[0]));
            chessBoard.setCurrentPlayer(parseColor(currentState[0].split(INFO_TAG)[POS_OF_CURRENT_PLAYER]));
        }
    }

    private GameState parseState(String stringState) throws ParseStateException {
        String[] infos = stringState.split(INFO_TAG);
        Color currentPlayer = parseColor(infos[1]);
        Quadruple quadruple = parseQuadruple(infos[POS_OF_QUAD]);
        LocalDateTime time = LocalDateTime.parse(infos[POS_OF_DATE_TIME]
                .substring(0, infos[POS_OF_DATE_TIME].length() - 2));
        Field[][] board = parseBoard(infos[POS_OF_BOARD]);

        return new GameState(board, currentPlayer, quadruple, time);

    }

    private Field[][] parseBoard(String rows) throws ParseStateException {
        String[] seperatedRows = rows.split("\n");
        Field[][] board = new Field[BOARD_SIZE][BOARD_SIZE];

        for (int i = 0; i < board.length; i++) {
            board[i] = parseRow(seperatedRows[i]);
        }
        return board;
    }

    private Color parseColor(String color) throws ParseStateException {
        if (color.equals("BLACK")) {
            return Color.BLACK;
        } else if (color.equals("WHITE")) {
            return Color.WHITE;
        } else {
            throw new ParseStateException();
        }
    }

    @SuppressWarnings("checkstyle:MagicNumber")
    private Quadruple parseQuadruple(String quadruple) {
        int yPosFrom = quadruple.charAt(1);
        int xPosFrom = quadruple.charAt(3);
        int yPosTo = quadruple.charAt(5);
        int xPosTo = quadruple.charAt(7);
        return new Quadruple(yPosFrom, xPosFrom, yPosTo, xPosTo);
    }

    private Field[] parseRow(String row) throws ParseStateException {
        Field[] chessSquares = new Field[BOARD_SIZE];
        String[] tuples = row.split("\\)");
        int k = 0;
        for (int i = 0; i < tuples.length; i++) {
            chessSquares[i] = tupleToSquare(tuples[i]);
        }
        return chessSquares;
    }


    private Field tupleToSquare(String tuple) throws ParseStateException {
        Field chessSquare = new Field();
        if (tuple.charAt(1) != 'x') {
            try {
                ChessMan chessMan = chessManFactory.charToChessSquare(tuple.charAt(1));
                if (tuple.charAt(2) == 'm') {
                    chessMan.setMoved(true);
                }

                chessSquare.addChessMan(chessMan);

            } catch (ParseException e) {
                throw new ParseStateException();
            }

        }

        return chessSquare;
    }
}
