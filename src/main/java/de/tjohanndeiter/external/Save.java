package de.tjohanndeiter.external;

import de.tjohanndeiter.model.chessmen.ChessMan;
import de.tjohanndeiter.model.GameManger;
import de.tjohanndeiter.model.Field;
import de.tjohanndeiter.model.GameState;
import de.tjohanndeiter.parser.CharCreator;
import de.tjohanndeiter.parser.fen.FenCharCreator;
import de.tjohanndeiter.utils.Quadruple;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;

public class Save {

    private static final String STATE_TAG = "<State>";
    private static final String INFO_TAG = "<Info>";
    private static final String CURRENT_STATE_TAG = "<CurrentState>";

    private CharCreator charCreator = new FenCharCreator();
    GameManger chessBoard;

    public Save(final GameManger chessBoard) {
        this.chessBoard = chessBoard;
    }

    public void createSaveFile(File file) throws IOException {
        if (file != null) {
            StringBuilder result = new StringBuilder();
            for (GameState state : chessBoard.getBackLog()) {
                result.append(STATE_TAG);
                result.append(parseState(state));
                result.append('\n');
            }
            result.append(CURRENT_STATE_TAG);
            result.append(boardToText(chessBoard.getCurrentBoard()));
            result.append(chessBoard.getCurrentPlayer().toString());
            result.append(INFO_TAG);
            result.append(LocalDateTime.now().toString());
            result.append(CURRENT_STATE_TAG);
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(result.toString());
            bufferedWriter.close();
        }
    }

    private String parseState(GameState state) {
        StringBuilder stateResult = new StringBuilder();
        Field[][] chessSquares = state.getBoard();
        stateResult.append(boardToText(chessSquares));
        stateResult.append(state.getColor().toString());
        stateResult.append(createTupleForQuadruple(state.getTurn()));
        stateResult.append(INFO_TAG);
        stateResult.append(state.getLocalDateTime().toString());
        return stateResult.toString();
    }

    private StringBuilder boardToText(Field[][] chessSquares) {
        StringBuilder board = new StringBuilder();
        for (Field[] chessSqauresInRow : chessSquares) {
            board.append(parseRow(chessSqauresInRow));
            board.append('\n');
        }
        board.append(INFO_TAG);
        return board;
    }

    private StringBuilder parseRow(Field... row) {
        StringBuilder rowResult = new StringBuilder();
        for (Field chessSquare : row) {
            rowResult.append(createTupleForSquare(chessSquare));
        }
        return rowResult;
    }

    private StringBuilder createTupleForSquare(Field chessSquare) {
        StringBuilder tuple = new StringBuilder("(");
        if (chessSquare.isEmpty()) {
            tuple.append('x');
        } else {
            tuple.append(charCreator.chessSquareToChar(chessSquare));

            tuple.append(',');
            ChessMan chessMan = chessSquare.getChessMan();
            if (chessMan.isMoved()) {
                tuple.append('m');
            } else {
                tuple.append('n');
            }
        }
        tuple.append(')');

        return tuple;
    }

    private StringBuilder createTupleForQuadruple(Quadruple quadruple) {
        StringBuilder tupleResult = new StringBuilder();
        tupleResult.append(INFO_TAG);
        tupleResult.append('(');
        tupleResult.append(quadruple.getYPosFrom());
        tupleResult.append(',');
        tupleResult.append(quadruple.getXPosFrom());
        tupleResult.append(',');
        tupleResult.append(quadruple.getYPosTo());
        tupleResult.append(',');
        tupleResult.append(quadruple.getXPosTo());
        tupleResult.append(')');
        return tupleResult;
    }
}
