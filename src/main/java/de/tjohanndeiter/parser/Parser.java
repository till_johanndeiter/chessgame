package de.tjohanndeiter.parser;

import de.tjohanndeiter.model.chessmen.Color;
import de.tjohanndeiter.exception.parse.ParseFieldException;
import de.tjohanndeiter.exception.parse.ParseMoveException;
import de.tjohanndeiter.model.GameManger;
import de.tjohanndeiter.model.Field;
import de.tjohanndeiter.parser.fen.FenParser;
import de.tjohanndeiter.utils.Quadruple;

/**
 * Parser Interface wich handels the conversion from a ChessNotation to the logic of the model.
 * Convert a string to an Array of {@link Field}'s an reverse
 * Convert a string to an List of {@link Quadruple}'s for move Figures an reverse
 * Current implementation {@link FenParser}
 */
public interface Parser {
    Field[][] stringToField(String input) throws ParseFieldException;
    String fieldToString(GameManger chessBoard);
    Color getTurn(String input) throws ParseFieldException;
    Quadruple stringToQuadruple(String input)  throws ParseMoveException;
    String quadrupleToString(Quadruple quadrupleToConvert) throws ParseMoveException;
    String getStandardStartNotation();
}
