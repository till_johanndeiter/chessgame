package de.tjohanndeiter.parser;

import de.tjohanndeiter.model.Field;


/**
 * Create a char for an chessMan.
 */
public interface CharCreator {
    char chessSquareToChar(Field chessSquare);
}
