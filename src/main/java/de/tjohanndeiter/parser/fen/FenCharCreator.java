package de.tjohanndeiter.parser.fen;

import de.tjohanndeiter.model.chessmen.Color;
import de.tjohanndeiter.model.chessmen.Piece;
import de.tjohanndeiter.model.Field;
import de.tjohanndeiter.parser.CharCreator;

public class FenCharCreator implements CharCreator {

    /**
     * Parse a {@link Field} to a char in a Fen Notation.
     *
     * @param chessSquare chessSquare to parse
     * @return char for a Fen Notation
     */
    public char chessSquareToChar(Field chessSquare) {
        char output = ' ';
        Piece piece = chessSquare.getChessMan().getPiece();

        switch (piece) {
            case ROOK:
                output = 'r';
                break;
            case KNIGHT:
                output = 'n';
                break;
            case BISHOP:
                output = 'b';
                break;
            case QUEEN:
                output = 'q';
                break;
            case KING:
                output = 'k';
                break;
            case PAWN:
                output = 'p';
                break;
        }

        Color color = chessSquare.getChessMan().getColor();

        if (color == Color.WHITE) {
            output = Character.toUpperCase(output);
        }

        return output;
    }
}
