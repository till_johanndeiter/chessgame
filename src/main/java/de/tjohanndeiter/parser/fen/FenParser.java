package de.tjohanndeiter.parser.fen;

import de.tjohanndeiter.exception.parse.ParseFieldException;
import de.tjohanndeiter.exception.parse.ParseMoveException;
import de.tjohanndeiter.model.GameManger;
import de.tjohanndeiter.model.Field;
import de.tjohanndeiter.parser.ChessManFactory;
import de.tjohanndeiter.parser.Parser;
import de.tjohanndeiter.utils.Quadruple;
import de.tjohanndeiter.utils.ValidationUtils;
import de.tjohanndeiter.model.chessmen.ChessMan;
import de.tjohanndeiter.model.chessmen.Color;

import java.util.List;

/**
 * Implementation of {@link Parser}
 * Handles the conversion for Fen-Notation to {@link GameManger} an reverse.
 */

public class FenParser implements Parser {

    private static final int SIZE_OF_BOARD = 8;
    private static final int LENGTH_OF_MOVE_CMD = 6;

    private ChessManFactory chessManFactory = new ChessManFactoryImpl();
    private FenCharCreator fenCharCreator = new FenCharCreator();


    /**
     * Convertes a Field in Fen Notation to an Array of {@link Field}.
     *
     * @param input Input String to convert
     * @return {@link Field[][]}  compatible wiht {@link GameManger}
     * @throws ParseFieldException if parsing get interrupt by wrong syntax
     */
    @Override
    public Field[][] stringToField(String input) throws ParseFieldException {
        String inputFieldCut = input.substring(0, input.length() - 2);
        final Field[][] chessSquares = emptyBoard();
        final String[] stringRows = inputFieldCut.split("/");
        if (stringRows.length != SIZE_OF_BOARD) {
            throw new ParseFieldException();
        }
        for (int i = 0; i < stringRows.length; i++) {
            parseRow(stringRows[i], chessSquares[i]);
        }

        if (!ValidationUtils.correctChessboard(chessSquares)) {
            throw new ParseFieldException();
        }

        return chessSquares;
    }

    /**
     * Parses a Row of a Fen Notation.
     *
     * @param row          Row String to parse
     * @param chessSquares Output compatible with {@link GameManger}
     * @throws ParseFieldException In case of a nov-valid Fen Row which result in {{@link #fieldToString(GameManger)}}
     *                             stop the parsing process.
     */
    private void parseRow(String row, Field... chessSquares) throws ParseFieldException {

        for (int i = 0; i < row.length(); i++) {
            if (Character.isDigit(row.charAt(i))) {
                i = i + Character.getNumericValue(row.charAt(i));
            } else {
                chessSquares[i].addChessMan(chessManFactory.charToChessSquare(row.charAt(i)));
            }
        }
    }

    /**
     * Creates an empty {@link GameManger} with different instances of {@link Field}.
     *
     * @return empty {@link GameManger}
     */
    private Field[][] emptyBoard() {
        Field[][] chessSquares = new Field[SIZE_OF_BOARD][SIZE_OF_BOARD];
        for (int i = 0; i < chessSquares.length; i++) {
            for (int j = 0; j < chessSquares.length; j++) {
                chessSquares[i][j] = new Field();
            }
        }

        return chessSquares;
    }

    /**
     * Methode for compatible with the divorce in the {@link GameManger} beetwenn the BOard and the current Player.
     *
     * @param chessBoard {@link GameManger} to parse to a Fen Notation
     * @return parsed Fen String
     */
    @Override
    public String fieldToString(GameManger chessBoard) {
        return fieldToString(chessBoard.getCurrentBoard(), chessBoard.getCurrentPlayer());
    }

    /**
     * Model-to-Text transformation for the {@link GameManger} to a FEN String with current Turn {@link Color}.
     *
     * @param chessSquares chessSquares with instances of {@link ChessMan}
     * @param player       {@link Color} with current Turn
     * @return Transformed String
     */
    private String fieldToString(Field[][] chessSquares, Color player) {
        final StringBuilder result = new StringBuilder();
        for (Field[] row : chessSquares) {
            result.append(rowToString(row));
        }
        if (player == Color.BLACK) {
            result.append(" b");
        } else {
            result.append(" w");
        }
        return result.toString();
    }

    /**
     * Parse a row represented by {@link Field[]} to a row of a Fen Notation.
     * Used in {{@link #fieldToString(GameManger)}}
     *
     * @param row row to parse
     * @return parsed String
     */
    private StringBuilder rowToString(Field... row) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < row.length; i++) {
            if (row[i].isEmpty()) {
                int countEmpty = 1;
                int j = i;
                while (j + 1 < SIZE_OF_BOARD && row[j + 1].isEmpty()) {
                    countEmpty++;
                    j++;
                }
                i = j;
                result.append(countEmpty);
            } else {
                result.append(fenCharCreator.chessSquareToChar(row[i]));
            }
        }
        result = result.append('/');
        return result;
    }

    /**
     * Parse the Color of the next Turn.
     *
     * @param input Fen String to Parse
     * @return {@link Color} of the current Turn for compatible with the {@link GameManger}
     */
    @Override
    public Color getTurn(String input) throws ParseFieldException {
        if (input.endsWith("w")) {
            return Color.WHITE;
        } else if (input.endsWith("b")) {
            return Color.BLACK;
        } else {
            throw new ParseFieldException();
        }
    }

    /**
     * Parse a List of Move Notations to a List of {@link Quadruple} for compatible with {@link GameManger ) moves}.
     *
     * @param input String of Move Commands in FEN Notation
     * @return {@link List<Quadruple>} wich are compatible witch {@link GameManger}
     * @throws ParseMoveException In case of a nov-valid Fen Row which result in {{@link #fieldToString(GameManger)}}
     *                            stop the parsing process
     */

    @SuppressWarnings("checkstyle:MagicNumber")
    @Override
    public Quadruple stringToQuadruple(String input) throws ParseMoveException {
        Quadruple coordinate = new Quadruple();
        if (input.length() == LENGTH_OF_MOVE_CMD && input.charAt(2) == '-') {
            coordinate.setXPosFrom(convertColumnNoation(input.charAt(0)));
            coordinate.setYPosFrom(convertRowNotation(input.charAt(1)));
            coordinate.setXPosTo(convertColumnNoation(input.charAt(3)));
            coordinate.setYPosTo(convertRowNotation(input.charAt(4)));
        } else {
            throw new ParseMoveException();
        }

        return coordinate;
    }

    /**
     * Convert a Column Char from a Fen Notation to an integer.
     *
     * @param row Char to parse
     * @return Integer for compatiblity with {@link Quadruple}
     * @throws ParseMoveException In case of a nov-valid Fen Move Command which result in
     *                            {{@link #stringToField(String)}} (ChessBoard)}} stop the parsing process.
     */
    @SuppressWarnings("checkstyle:MagicNumber")
    private int convertColumnNoation(char row) throws ParseMoveException {
        switch (row) {
            case 'a':
                return 0;
            case 'b':
                return 1;
            case 'c':
                return 2;
            case 'd':
                return 3;
            case 'e':
                return 4;
            case 'f':
                return 5;
            case 'g':
                return 6;
            case 'h':
                return 7;
            default:
                throw new ParseMoveException();

        }
    }

    /**
     * @param row Char in the Fen Notation which describe a Row on the Board.
     * @return integer for compatibility with {@link Quadruple}
     * @throws ParseMoveException In case of a nov-valid Fen Move Command which result in
     *                            stop the parsing process
     */
    private int convertRowNotation(char row) throws ParseMoveException {
        if (Character.getNumericValue(row) < SIZE_OF_BOARD + 1 && Character.getNumericValue(row) > -1) {
            return SIZE_OF_BOARD - Character.getNumericValue(row);
        } else {
            throw new ParseMoveException();
        }
    }

    @Override
    public String quadrupleToString(Quadruple coordinatesToConvert) throws ParseMoveException {
        String result = new String();
        result = result + (columnToFen(coordinatesToConvert.getXPosFrom()));
        result = result + (rowToFen(coordinatesToConvert.getYPosFrom()));
        result = result + ('-');
        result = result + (columnToFen(coordinatesToConvert.getXPosTo()));
        result = result + (rowToFen(coordinatesToConvert.getYPosTo()));
        result = result + ';';
        return result;
    }

    @SuppressWarnings("checkstyle:MagicNumber")
    private char columnToFen(int row) throws ParseMoveException {
        switch (row) {
            case 0:
                return 'a';
            case 1:
                return 'b';
            case 2:
                return 'c';
            case 3:
                return 'd';
            case 4:
                return 'e';
            case 5:
                return 'f';
            case 6:
                return 'g';
            case 7:
                return 'h';
            default:
                throw new ParseMoveException();

        }
    }

    private char rowToFen(int row) throws ParseMoveException {
        if (row < SIZE_OF_BOARD + 1 && row > -1) {
            return Character.forDigit(SIZE_OF_BOARD - row, 10);
        } else {
            throw new ParseMoveException();
        }
    }

    @Override
    public String getStandardStartNotation() {
        return "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w";
    }
}
