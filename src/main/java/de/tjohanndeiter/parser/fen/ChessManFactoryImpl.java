package de.tjohanndeiter.parser.fen;


import de.tjohanndeiter.model.chessmen.ChessMan;
import de.tjohanndeiter.model.chessmen.Piece;
import de.tjohanndeiter.model.chessmen.Color;
import de.tjohanndeiter.exception.parse.ParseFieldException;
import de.tjohanndeiter.parser.ChessManFactory;

public class ChessManFactoryImpl implements ChessManFactory {

    @Override
    public ChessMan charToChessSquare(char charInFenSequence) throws ParseFieldException {
        Color color = Character.isLowerCase(charInFenSequence) ? Color.BLACK : Color.WHITE;
        Piece chessPiece;
        final char fen = Character.toLowerCase(charInFenSequence);
        switch (fen) {
            case 'r':
                chessPiece = Piece.ROOK;
                break;
            case 'n':
                chessPiece = Piece.KNIGHT;
                break;
            case 'b':
                chessPiece = Piece.BISHOP;
                break;
            case 'q':
                chessPiece = Piece.QUEEN;
                break;
            case 'k':
                chessPiece = Piece.KING;
                break;
            case 'p':
                chessPiece = Piece.PAWN;
                break;
            default:
                throw new ParseFieldException();
        }
        return new ChessMan(color, chessPiece);
    }
}
