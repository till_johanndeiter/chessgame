package de.tjohanndeiter.parser;

import de.tjohanndeiter.model.chessmen.ChessMan;
import de.tjohanndeiter.exception.parse.ParseFieldException;

public interface ChessManFactory {
    ChessMan charToChessSquare(char charInFenSequenc) throws ParseFieldException;
}
