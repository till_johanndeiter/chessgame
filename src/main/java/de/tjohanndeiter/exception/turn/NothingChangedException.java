package de.tjohanndeiter.exception.turn;

/**
 * Exeption die geworfen wird wenn keine veränderung stattfindet.
 */

public class NothingChangedException extends TurnException {

    private static final long serialVersionUID = 8582433437601788991L;

    private static final int ERROR_CODE = 104;

    public NothingChangedException() {
        super(ERROR_CODE);
    }
}
