package de.tjohanndeiter.exception.turn;

import de.tjohanndeiter.exception.ChessGameException;

public abstract class TurnException extends ChessGameException {

    private static final long serialVersionUID = 8582433437601788991L;

    TurnException(final int errorCode) {
        super(errorCode);
    }
}

