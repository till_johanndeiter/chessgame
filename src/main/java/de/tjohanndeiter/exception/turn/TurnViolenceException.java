package de.tjohanndeiter.exception.turn;

/**
 * Exeption die geworfen wird wenn gegen Zugrecht verstoßen wird.
 */

public class TurnViolenceException extends TurnException {

    private static final long serialVersionUID = 8582433437601788991L;

    private static final int ERROR_CODE = 103;

    public TurnViolenceException() {
        super(ERROR_CODE);
    }
}
