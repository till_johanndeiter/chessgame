package de.tjohanndeiter.exception.turn;

public class InValidMoveException extends TurnException {

    private static final long serialVersionUID = 8582433437601788991L;

    private static final int ERROR_CODE = 110;

    public InValidMoveException() {
            super(ERROR_CODE);
        }
}
