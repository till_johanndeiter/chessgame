package de.tjohanndeiter.exception.turn;

/**
 * Exeption die geworfen wird wenn das de.techfak.gse.tjohanndeiter.Feld leer ist.
 */

public class EmptyFieldException extends TurnException {

    private static final int ERROR_CODE = 102;

    private static final long serialVersionUID = 8582433437601788991L;


    public EmptyFieldException() {
        super(ERROR_CODE);
    }
}
