package de.tjohanndeiter.exception.parse;

public class ParseFieldException extends ParseException {

    private static final long serialVersionUID = 8582433437601788991L;

    private static final int ERROR_CODE = 100;

    public ParseFieldException() {
        super(ERROR_CODE);
    }
}
