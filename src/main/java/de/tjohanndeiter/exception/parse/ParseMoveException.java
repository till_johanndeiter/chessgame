package de.tjohanndeiter.exception.parse;

public class ParseMoveException extends ParseException {

    private static final long serialVersionUID = 8582433437601788991L;

    private static final int ERROR_CODE = 101;

    public ParseMoveException() {
            super(ERROR_CODE);
        }
}
