package de.tjohanndeiter.exception.parse;

public class ParseStateException extends ParseException {

    private static final long serialVersionUID = 8582433437601788991L;

    private static final int ERROR_CODE = 150;

    public ParseStateException() {
        super(ERROR_CODE);
    }
}
