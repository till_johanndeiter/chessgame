package de.tjohanndeiter.exception.parse;

import de.tjohanndeiter.exception.ChessGameException;

public abstract class ParseException extends ChessGameException {

    private static final long serialVersionUID = 8582433437601788991L;

    ParseException(final int errorCode) {
        super(errorCode);
    }
}
