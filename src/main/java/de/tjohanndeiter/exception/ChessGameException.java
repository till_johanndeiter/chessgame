package de.tjohanndeiter.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public abstract class ChessGameException extends Exception {

    private static final long serialVersionUID = 8582433437601788991L;

    protected int errorCode;

    public ChessGameException(int errorCode) {
        this.errorCode = errorCode;
    }
}
