package de.tjohanndeiter.exception.ki;

import de.tjohanndeiter.exception.ChessGameException;

public class KiException extends ChessGameException {

    private static final long serialVersionUID = 8582433437601788991L;

    private static final int ERROR_CODE = 115;

    public KiException() {
        super(ERROR_CODE);
    }
}
