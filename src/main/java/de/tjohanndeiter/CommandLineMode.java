package de.tjohanndeiter;

import de.tjohanndeiter.exception.ChessGameException;
import de.tjohanndeiter.exception.parse.ParseFieldException;
import de.tjohanndeiter.exception.turn.TurnException;
import de.tjohanndeiter.model.GameManger;
import de.tjohanndeiter.model.moveOptions.OptionCalculator;
import de.tjohanndeiter.model.moveOptions.StandardOptionCalculator;
import de.tjohanndeiter.parser.Parser;
import de.tjohanndeiter.parser.fen.FenParser;
import de.tjohanndeiter.player.CommandLineHuman;
import de.tjohanndeiter.player.Player;
import de.tjohanndeiter.utils.Quadruple;
import lombok.Data;

/**
 * Implementation of the {@link Mode} which uses the commandline for interaction with the game.
 * For game interaction e.g chess notations with the model it uses the {{@link #parser}}.
 */
@Data
public class CommandLineMode implements Mode {

    private GameManger chessBoard;
    private OptionCalculator optionCalculator;
    private Parser parser;
    private Player playerOne;
    private Player playerTwo;

    private CommandLineMode(final Parser parser) {
        this.parser = parser;
        chessBoard = new GameManger(new StandardOptionCalculator());
        playerOne = new CommandLineHuman(parser);
        playerTwo = new CommandLineHuman(parser);
    }

    CommandLineMode() {
        this(new FenParser());
    }

    /**
     * Try to init the game with the start notification from the program args else uses standard Start Notation.
     * TODO outsource the standard start notification the the implemented parser
     *
     * @param startNotation Notation used for start the game
     */
    @Override
    public void startGame(final String startNotation) {

        try {
            if (startNotation == null) {
                initializeBoard(parser.getStandardStartNotation());
            } else {
                initializeBoard(startNotation);
            }
            gameLoop();
        } catch (ChessGameException e) {
            System.exit(e.getErrorCode());
        }

    }

    private void gameLoop() throws TurnException {
        Quadruple quadruple = new Quadruple();
        boolean inGame = true;
        do {
            quadruple = playerOne.move(chessBoard.getBoard(), quadruple);
            tryMove(quadruple);
            quadruple = playerTwo.move(chessBoard.getBoard(), quadruple);
            tryMove(quadruple);
        } while (inGame);
    }

    private void tryMove(final Quadruple coordinate) throws TurnException {
        chessBoard.tryMove(coordinate.getYPosFrom(), coordinate.getXPosFrom(),
            coordinate.getYPosTo(), coordinate.getXPosTo());
        System.out.println(parser.fieldToString(chessBoard));
    }

    private void initializeBoard(final String startFen) throws ParseFieldException {
        chessBoard.setCurrentPlayer(parser.getTurn(startFen));
        chessBoard.setBoard(parser.stringToField(startFen));
    }


}
