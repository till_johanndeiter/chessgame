package de.tjohanndeiter.player;

import de.tjohanndeiter.model.chessmen.Color;
import de.tjohanndeiter.model.GameManger;
import de.tjohanndeiter.model.Field;
import de.tjohanndeiter.utils.Quadruple;

import java.util.List;

/**
 * Class for generate {@link List<Quadruple>} for compatibility with the
 * {@link GameManger} with a Chess-Ki.
 */
public abstract class Computer extends Player {

    public Computer(Color color) {
        super(color);
    }

    @Override
    public abstract Quadruple move(Field[][] board, Quadruple movesFromOther);

}
