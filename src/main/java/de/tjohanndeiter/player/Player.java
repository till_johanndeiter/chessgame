package de.tjohanndeiter.player;

import de.tjohanndeiter.Mode;
import de.tjohanndeiter.model.chessmen.Color;
import de.tjohanndeiter.model.Field;
import de.tjohanndeiter.utils.Quadruple;
import lombok.Data;

import java.util.List;

/**
 * Abstract Class which represent a Player Human oder Non-Human. Has his {{@link #color}} in the Game an
 * returns a {@link List<Quadruple>} with {{@link #move(Field[][], Quadruple)} used in the gameLoop in the
 * {@link Mode}
 */

@Data
public abstract class Player {

    private Color color;

    public Player(Color color) {
        this.color = color;
    }

    public abstract Quadruple move(Field[][] board, Quadruple movesFromOther);

}
