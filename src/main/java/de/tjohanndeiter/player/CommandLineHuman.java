package de.tjohanndeiter.player;

import de.tjohanndeiter.CommandLineMode;
import de.tjohanndeiter.model.chessmen.Color;
import de.tjohanndeiter.exception.parse.ParseException;
import de.tjohanndeiter.model.GameManger;
import de.tjohanndeiter.model.Field;
import de.tjohanndeiter.parser.Parser;
import de.tjohanndeiter.utils.Quadruple;

import java.util.List;
import java.util.Scanner;


/**
 * Represents a Human Player who do his Input with the CommandLine with the {@link Scanner}.
 */
public class CommandLineHuman extends Player {

    private Parser parser;
    private Scanner sc = new Scanner(System.in);


    public CommandLineHuman(Parser parser) {
        super(Color.WHITE);
        this.parser = parser;
    }

    /**
     * Create a {@link List} in a Notation uses by a Implementation of a {@link Parser} of {@link Quadruple} for
     * compatibility with the {@link GameManger}
     * @return Move Quadruple uses {@link CommandLineMode}the gameLoop.
     */
    @Override
    public Quadruple move(Field[][] board, Quadruple movesFromOther) {
        Quadruple quadruple = new Quadruple();
        String userInput = sc.nextLine();
        try {
            quadruple = parser.stringToQuadruple(userInput);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return quadruple;
    }
}
