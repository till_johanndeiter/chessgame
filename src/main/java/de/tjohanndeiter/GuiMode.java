package de.tjohanndeiter;


import de.tjohanndeiter.controller.MenuController;
import de.tjohanndeiter.parser.Parser;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;

public class GuiMode extends Application implements Mode {


    @Override
    public void start(final Stage stage) throws IOException {

        final FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("MainMenu.fxml"));
        final Pane root = fxmlLoader.load();
        final MenuController menuController = fxmlLoader.getController();
        final Scene scene = new Scene(root);
        stage.setTitle("ChessGame");
        stage.setScene(scene);
        stage.show();
    }


    //TODO implement parse of the start notation.
    /**
     * Simple implementation method for communication with the {@link ChessGame}.
     * @param start Chessfield at start in a chessnotation
     * {@link Parser}
     */
    public void startGame(final String start) {
        launch(start);
    }
}

