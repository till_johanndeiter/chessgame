package de.tjohanndeiter.model;

public enum MoveAbility {
    LEGAL, ILLEGAL, ATTACK;
}
