package de.tjohanndeiter.model;

import de.tjohanndeiter.model.chessmen.ChessMan;
import lombok.Data;

//

@Data
public class Field {
    private ChessMan chessMan;

    public ChessMan getChessMan() {
        return chessMan;
    }

    public void addChessMan(final ChessMan chessMan) {
        this.chessMan = chessMan;
    }

    void removeChessMan() {
        chessMan = null;
    }

    public boolean isEmpty() {
        return chessMan == null;
    }
}
