package de.tjohanndeiter.model;

import de.tjohanndeiter.model.chessmen.Color;
import de.tjohanndeiter.exception.turn.TurnException;
import de.tjohanndeiter.model.moveOptions.OptionCalculator;
import de.tjohanndeiter.player.Computer;
import de.tjohanndeiter.utils.Quadruple;


//TODO: Remove and move computer move to own class

public class ChessBoardWithComputer extends GameManger {

    private Computer computer;

    public ChessBoardWithComputer(Computer computer, OptionCalculator optionCalculator) {
        super(optionCalculator);
        this.computer = computer;
    }

    public ChessBoardWithComputer(Computer computer, OptionCalculator optionCalculator, Color currentPlayer) {
        super(optionCalculator, currentPlayer);
        this.computer = computer;
    }

    public void tryMove(final int yPosFrom, final int xPosFrom,
                        final int yPosTo, final int xPosTo) throws TurnException {
        super.tryMove(yPosFrom, xPosFrom, yPosTo, xPosTo);
        Quadruple quadruple = new Quadruple(yPosFrom, xPosFrom, yPosTo, xPosTo);
        moveByComputer(quadruple);
    }

    public void moveByComputer(Quadruple quadruple) throws TurnException {
        quadruple = computer.move(getBoard(), quadruple);
        super.tryMove(quadruple.getYPosFrom(), quadruple.getXPosFrom(), quadruple.getYPosTo(), quadruple.getXPosTo());
        support.firePropertyChange(BORD_CHANGED, null, quadruple);
    }

}
