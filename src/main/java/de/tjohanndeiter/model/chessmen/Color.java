package de.tjohanndeiter.model.chessmen;

public enum Color {
    BLACK, WHITE;
}
