package de.tjohanndeiter.model.chessmen;

public enum Piece {
    PAWN, KING, KNIGHT, BISHOP, ROOK, QUEEN
}
