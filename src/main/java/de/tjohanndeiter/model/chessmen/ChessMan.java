package de.tjohanndeiter.model.chessmen;


import lombok.Getter;

@Getter
public class ChessMan {

    private Color color;
    private de.tjohanndeiter.model.chessmen.Piece Piece;
    private boolean moved = false;

    public ChessMan(final Color color, final de.tjohanndeiter.model.chessmen.Piece Piece) {
        this.color = color;
        this.Piece = Piece;
    }

    public boolean isMoved() {
        return moved;
    }

    public void setMoved(final boolean moved) {
        this.moved = moved;
    }
}


