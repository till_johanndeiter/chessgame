package de.tjohanndeiter.model;

import de.tjohanndeiter.model.chessmen.Color;
import de.tjohanndeiter.utils.Quadruple;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class GameState {

    private Field[][] board;
    private Color color;
    private Quadruple turn;
    private LocalDateTime localDateTime;

    private GameState(Field[][] board, Color color) {
        this.board = board.clone();
        this.color = color;
        localDateTime = LocalDateTime.now();
    }


    GameState(Field[][] board, Color color, Quadruple turn) {
        this(board, color);
        this.turn = turn;
    }
}
