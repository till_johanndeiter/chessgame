package de.tjohanndeiter.model.moveOptions;

import de.tjohanndeiter.model.Field;
import de.tjohanndeiter.model.MoveAbility;

public interface OptionCalculator {

    int CHESSBOARD_SIZE = 8;
    MoveAbility[][] getMoveOptions(Field[][] board, int yPosFrom, int xPosFrom);
}
