package de.tjohanndeiter.model.moveOptions;

import de.tjohanndeiter.model.Field;
import de.tjohanndeiter.model.MoveAbility;
import de.tjohanndeiter.model.chessmen.ChessMan;
import de.tjohanndeiter.model.chessmen.Color;
import de.tjohanndeiter.model.chessmen.Piece;

//TODO: PAWN CAN BEAT TWO STEPS!!! May edit try beat to tryMoveInDirection
//TODO: Write Docu for class
public class StandardOptionCalculator implements OptionCalculator {

    private static final int UP = -1;
    private static final int DOWN = 1;
    private static final int LEFT = -1;
    private static final int RIGHT = 1;
    private static final int SAME = 0;
    private static final int[] JUMP_WIDTH = {-2, -1, 1, 2};
    private static final int[] AROUND_POS = {-1, 0, 1};

    @Override
    public MoveAbility[][] getMoveOptions(final Field[][] board, final int yPosFrom, final int xPosFrom) {
        final MoveAbility[][] moveAbilities = new MoveAbility[CHESSBOARD_SIZE][CHESSBOARD_SIZE];
        illegal(moveAbilities);
        final ChessMan chessMan = board[yPosFrom][xPosFrom].getChessMan();
        final Piece piece = chessMan.getPiece();
        switch (piece) {
            case KING:
                tryMoveAround(board, moveAbilities, yPosFrom, xPosFrom);
                break;
            case PAWN:
                if (chessMan.isMoved()) {
                    tryMoveForward(board, moveAbilities, yPosFrom, xPosFrom);
                } else {
                    tryPositionsForFirstMove(board, moveAbilities, yPosFrom, xPosFrom);
                }
                break;
            case ROOK:
                tryHorizontal(board, moveAbilities, yPosFrom, xPosFrom);
                tryVertical(board, moveAbilities, yPosFrom, xPosFrom);
                break;
            case QUEEN:
                tryVertical(board, moveAbilities, yPosFrom, xPosFrom);
                tryHorizontal(board, moveAbilities, yPosFrom, xPosFrom);
                tryDiagonal(board, moveAbilities, yPosFrom, xPosFrom);
                break;
            case BISHOP:
                tryDiagonal(board, moveAbilities, yPosFrom, xPosFrom);
                break;
            case KNIGHT:
                tryJump(board, moveAbilities, yPosFrom, xPosFrom);
                break;
        }

        return moveAbilities;
    }

    private void tryMoveAround(final Field[][] board, final MoveAbility[][] moveAbilities, final int yPosFrom,
                               final int xPosFrom) {
        final Color color = board[yPosFrom][xPosFrom].getChessMan().getColor();
        for (final int aroundXPos : AROUND_POS) {
            for (final int aroundYPos : AROUND_POS) {
                int yPosToCheck = yPosFrom + aroundXPos;
                int xPosToCheck = xPosFrom + aroundYPos;
                if (inField(yPosToCheck, xPosToCheck)) {
                    if (board[yPosToCheck][xPosToCheck].isEmpty()) {
                        moveAbilities[yPosToCheck][xPosToCheck] = MoveAbility.LEGAL;
                    } else if (board[yPosToCheck][xPosToCheck].getChessMan().getColor() != color) {
                        moveAbilities[yPosToCheck][xPosToCheck] = MoveAbility.ATTACK;
                    }
                }
            }
        }
    }

    private void tryJump(final Field[][] board, final MoveAbility[][] moveAbilities, final int yPosFrom,
                         final int xPosFrom) {
        for (final int xPosAddition : JUMP_WIDTH) {
            for (final int yPosAddition : JUMP_WIDTH) {
                final int xPosTo = xPosFrom + xPosAddition;
                final int yPosTo = yPosFrom + yPosAddition;
                if (Math.abs(xPosAddition) != Math.abs(yPosAddition) && inField(yPosTo, xPosTo)) {
                    final Field chessSquare = board[yPosTo][xPosTo];
                    if (!chessSquare.isEmpty()) {
                        final ChessMan chessMan = board[yPosTo][xPosTo].getChessMan();
                        if (chessMan.getColor() != board[yPosFrom][xPosFrom].getChessMan().getColor()) {
                            moveAbilities[yPosTo][xPosTo] = MoveAbility.ATTACK;
                        }
                    } else {
                        moveAbilities[yPosTo][xPosTo] = MoveAbility.LEGAL;
                    }
                }
            }
        }
    }

    private void illegal(MoveAbility[]... moveAbilities) {
        for (int i = 0; i < moveAbilities.length; i++) {
            for (int j = 0; j < moveAbilities.length; j++) {
                moveAbilities[i][j] = MoveAbility.ILLEGAL;
            }
        }
    }

    private void tryMoveForward(Field[][] board, MoveAbility[][] moveAbilities, int yPosFrom,
                                int xPosFrom) {

        if (board[yPosFrom][xPosFrom].getChessMan().getColor() == Color.BLACK) {
            tryMoveInDirection(board, moveAbilities, yPosFrom, xPosFrom, DOWN, SAME, 1, false);
            tryBeatInDirection(board, moveAbilities, yPosFrom, xPosFrom, DOWN, RIGHT);
            tryBeatInDirection(board, moveAbilities, yPosFrom, xPosFrom, DOWN, LEFT);
        } else {
            tryMoveInDirection(board, moveAbilities, yPosFrom, xPosFrom, UP, SAME, 1, false);
            tryBeatInDirection(board, moveAbilities, yPosFrom, xPosFrom, UP, RIGHT);
            tryBeatInDirection(board, moveAbilities, yPosFrom, xPosFrom, UP, LEFT);
        }
    }

    private void tryBeatInDirection(Field[][] board, MoveAbility[][] moveAbilities, int yPosFrom,
                                    int xPosFrom, int yPosDirection, int xPosDirection) {
        final Color currentPlayer = board[yPosFrom][xPosFrom].getChessMan().getColor();
        final int yPosFieldToCheck = yPosFrom + yPosDirection;
        final int xPosFieldToCheck = xPosFrom + xPosDirection;
        if (inField(yPosFieldToCheck, xPosFieldToCheck)) {
            if (!board[yPosFieldToCheck][xPosFieldToCheck].isEmpty() &&
                    board[yPosFieldToCheck][xPosFieldToCheck].getChessMan().getColor() != currentPlayer) {
                moveAbilities[yPosFrom + yPosDirection][xPosFrom + xPosDirection] = MoveAbility.ATTACK;
            }
        }
    }

    private void tryPositionsForFirstMove(Field[][] board, MoveAbility[][] moveAbilities,
                                          int yPosFrom, int xPosFrom) {
        final ChessMan chessMan = board[yPosFrom][xPosFrom].getChessMan();
        if (!chessMan.isMoved()) {
            if (board[yPosFrom][xPosFrom].getChessMan().getColor() == Color.BLACK) {
                tryMoveInDirection(board, moveAbilities, yPosFrom, xPosFrom, DOWN, SAME, 2, false);
                tryBeatInDirection(board, moveAbilities, yPosFrom, xPosFrom, DOWN, RIGHT);
                tryBeatInDirection(board, moveAbilities, yPosFrom, xPosFrom, DOWN, LEFT);
            } else {
                tryMoveInDirection(board, moveAbilities, yPosFrom, xPosFrom, UP, SAME, 2, false);
                tryBeatInDirection(board, moveAbilities, yPosFrom, xPosFrom, UP, RIGHT);
                tryBeatInDirection(board, moveAbilities, yPosFrom, xPosFrom, UP, LEFT);
            }
        }
    }

    private void tryVertical(Field[][] board, MoveAbility[][] moveAbilities,
                             int yPosFrom, int xPosFrom) {
        tryMoveInDirection(board, moveAbilities, yPosFrom, xPosFrom, SAME, RIGHT, CHESSBOARD_SIZE);
        tryMoveInDirection(board, moveAbilities, yPosFrom, xPosFrom, SAME, LEFT, CHESSBOARD_SIZE);
    }

    private void tryHorizontal(Field[][] board, MoveAbility[][] moveAbilities,
                               int yPosFrom, int xPosFrom) {
        tryMoveInDirection(board, moveAbilities, yPosFrom, xPosFrom, DOWN, SAME, CHESSBOARD_SIZE);
        tryMoveInDirection(board, moveAbilities, yPosFrom, xPosFrom, UP, SAME, CHESSBOARD_SIZE);
    }

    private void tryDiagonal(Field[][] board, MoveAbility[][] moveAbilities,
                             int yPosFrom, int xPosFrom) {
        tryMoveInDirection(board, moveAbilities, yPosFrom, xPosFrom, DOWN, RIGHT, CHESSBOARD_SIZE);
        tryMoveInDirection(board, moveAbilities, yPosFrom, xPosFrom, UP, RIGHT, CHESSBOARD_SIZE);
        tryMoveInDirection(board, moveAbilities, yPosFrom, xPosFrom, DOWN, LEFT, CHESSBOARD_SIZE);
        tryMoveInDirection(board, moveAbilities, yPosFrom, xPosFrom, UP, LEFT, CHESSBOARD_SIZE);
    }

    private void tryMoveInDirection(Field[][] board, MoveAbility[][] moveAbilities,
                                    int yPosFrom, int xPosFrom, int yDirection, int xDirection,
                                    int maxStepSize) {
        tryMoveInDirection(board, moveAbilities, yPosFrom, xPosFrom, yDirection, xDirection, maxStepSize,
                true);
    }

    private void tryMoveInDirection(Field[][] board, MoveAbility[][] moveAbilities,
                                    int yPosFrom, int xPosFrom, int yDirection, int xDirection,
                                    int maxStepSize, boolean beat) {
        int countSteps = 0;
        int yPosFieldToCheck = yPosFrom + yDirection;
        int xPosFieldToCheck = xPosFrom + xDirection;
        final ChessMan currentChessMan = board[yPosFrom][xPosFrom].getChessMan();
        while (inField(yPosFieldToCheck, xPosFieldToCheck) &&
                countSteps < maxStepSize) {
            if (board[yPosFieldToCheck][xPosFieldToCheck].isEmpty()) {
                moveAbilities[yPosFieldToCheck][xPosFieldToCheck] = MoveAbility.LEGAL;
                yPosFieldToCheck = yPosFieldToCheck + yDirection;
                xPosFieldToCheck = xPosFieldToCheck + xDirection;
                countSteps++;
            } else if (board[yPosFieldToCheck][xPosFieldToCheck].getChessMan().getColor()
                    != currentChessMan.getColor()) {
                if (beat) {
                    moveAbilities[yPosFieldToCheck][xPosFieldToCheck] = MoveAbility.ATTACK;
                }
                break;
            } else {
                break;
            }
        }
    }

    private boolean inField(int yPos, int xPos) {
        return 0 <= yPos && yPos < CHESSBOARD_SIZE &&
                0 <= xPos && xPos < CHESSBOARD_SIZE;
    }
}
