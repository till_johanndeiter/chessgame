package de.tjohanndeiter.model.moveOptions;

import de.tjohanndeiter.model.MoveAbility;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class MoveAbbilities {
    private MoveAbility[][] moveAbilities;
}
