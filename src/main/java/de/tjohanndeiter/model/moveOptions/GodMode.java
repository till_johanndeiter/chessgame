package de.tjohanndeiter.model.moveOptions;

import de.tjohanndeiter.model.Field;
import de.tjohanndeiter.model.MoveAbility;

import java.util.Arrays;

public class GodMode implements OptionCalculator {

    @Override
    public MoveAbility[][] getMoveOptions(final Field[][] board, final int yPosFrom, final int xPosFrom) {
        final MoveAbility[] tempAbilities = new MoveAbility[CHESSBOARD_SIZE];
        Arrays.fill(tempAbilities, MoveAbility.LEGAL);
        final MoveAbility[][] moveAbilities = new MoveAbility[CHESSBOARD_SIZE][CHESSBOARD_SIZE];
        Arrays.fill(moveAbilities, tempAbilities);
        return moveAbilities;
    }
}
