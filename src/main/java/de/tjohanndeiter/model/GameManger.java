package de.tjohanndeiter.model;

import de.tjohanndeiter.model.chessmen.ChessMan;
import de.tjohanndeiter.model.chessmen.Color;
import de.tjohanndeiter.exception.turn.EmptyFieldException;
import de.tjohanndeiter.exception.turn.InValidMoveException;
import de.tjohanndeiter.exception.turn.NothingChangedException;
import de.tjohanndeiter.exception.turn.TurnException;
import de.tjohanndeiter.exception.turn.TurnViolenceException;
import de.tjohanndeiter.model.moveOptions.OptionCalculator;
import de.tjohanndeiter.utils.GeneratorUtils;
import de.tjohanndeiter.utils.Quadruple;
import de.tjohanndeiter.utils.TArrayList;
import lombok.Getter;
import lombok.Setter;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;

/**
 * Model of the Chessboard. Main Class of the Game logic. Uses as representation of the ChessBoard the
 * Data Structure {{@link #board}} which is an two-dimensional array of {@link Field[][]}. That Contains a
 * {@link TArrayList <ChessMan>} for better reuse with games that need a three Dimensional Game Board.
 */
@Getter
@Setter
public class GameManger {

    public static final String BORD_CHANGED = "BoardChange";

    private static final int SIZE_OF_BOARD = 8;

    protected PropertyChangeSupport support = new PropertyChangeSupport(this);

    private Field[][] board;
    private List<GameState> backLog = new TArrayList<>();
    private OptionCalculator optionCalculator;
    private Color currentPlayer;
    private List<ChessMan> whiteAlive = new TArrayList<>();
    private List<ChessMan> blackAlive = new TArrayList<>();
    private List<ChessMan> whiteKilled = new TArrayList<>();
    private List<ChessMan> blackKilled = new TArrayList<>();

    public GameManger(final OptionCalculator optionCalculator) {
        this.optionCalculator = optionCalculator;
        currentPlayer = Color.WHITE;
        board = GeneratorUtils.standardBegin();
        for (int i = 0; i < SIZE_OF_BOARD; i++) {
            for (int j = 0; j < SIZE_OF_BOARD; j++) {
                if (!board[i][j].isEmpty()) {
                    final ChessMan chessMan = board[i][j].getChessMan();
                    if (board[i][j].getChessMan().getColor() == Color.WHITE) {
                        whiteAlive.add(chessMan);
                    } else {
                        blackAlive.add(chessMan);
                    }
                }
            }
        }
    }


    public GameManger(OptionCalculator optionCalculator, Color currentPlayer) {
        this(optionCalculator);
        this.currentPlayer = currentPlayer;
    }


    public void addObserver(final PropertyChangeListener observer) {
        support.addPropertyChangeListener(observer);
    }

    public Field[][] getCurrentBoard() {
        return board;
    }

    //TODO: Add Exception for chessMan
    public ChessMan getChessMan(int yPos, int xPos) {
        if (board[yPos][xPos].isEmpty()) {
            return null;
        } else {
            return board[yPos][xPos].getChessMan();
        }
    }


    /**
     * Try to move a ChessMan from one to another ChessSquare
     *
     * @param yPosFrom yPos of the {@link ChessMan}
     * @param xPosFrom xPos of the ChessMan {@link ChessMan}
     * @param yPosTo   yPos of the target {@link Field}
     * @param xPosTo   xPos of the target {@link Quadruple}
     * @throws EmptyFieldException     in case of an empty target {@link Field}
     * @throws TurnViolenceException   in case of {{@link #currentPlayer}} not equals {@link Color}
     *                                 of {@link ChessMan}
     * @throws NothingChangedException in case of target {@link Field} equals chessman {@link Field}
     * @throws InValidMoveException    in case of an move try not validated by {{@link #optionCalculator}}
     */
    public void tryMove(final int yPosFrom, final int xPosFrom,
                        final int yPosTo, final int xPosTo) throws TurnException {
        if (board[yPosFrom][xPosFrom].isEmpty()) {
            throw new EmptyFieldException();
        } else if (currentPlayer != board[yPosFrom][xPosFrom].getChessMan().getColor()) {
            throw new TurnViolenceException();
        } else if (yPosFrom == yPosTo && xPosFrom == xPosTo) {
            throw new NothingChangedException();
        } else if (!validateMove(yPosFrom, xPosFrom, yPosTo, xPosTo)) {
            throw new InValidMoveException();
        } else {
            doValidMove(yPosFrom, xPosFrom, yPosTo, xPosTo);
        }
    }


    /**
     * Calculate with the {{@link #optionCalculator}} all MoveOptions of the current {@link ChessMan}
     * and checks if the traget {@link Field} is marked
     * as LEGAL OR ATTACK.
     *
     * @param yPosFrom yPos of the {@link ChessMan}
     * @param xPosFrom xPos of the ChessMan {@link ChessMan}
     * @param yPosTo   yPos of the target {@link Field}
     * @param xPosTo   xPos of the target {@link Quadruple}
     * @return true if valid move else false
     */
    private boolean validateMove(final int yPosFrom, final int xPosFrom,
                                 final int yPosTo, final int xPosTo) {
        final MoveAbility[][] moveAbilities = optionCalculator.getMoveOptions(board, yPosFrom,
                xPosFrom);
        return moveAbilities[yPosTo][xPosTo] != MoveAbility.ILLEGAL;
    }

    /**
     * Execute if Move of a {@link ChessMan} to the target {@link Field}.
     *
     * @param yPosFrom yPos of the {@link ChessMan}
     * @param xPosFrom xPos of the ChessMan {@link ChessMan}
     * @param yPosTo   yPos of the target {@link Field}
     * @param xPosTo   xPos of the target {@link Quadruple}
     */
    private void doValidMove(final int yPosFrom, final int xPosFrom, final int yPosTo, final int xPosTo) {
        Quadruple turn = new Quadruple(yPosFrom, xPosFrom, yPosTo, xPosTo);
        backLog.add(new GameState(board, currentPlayer, turn));
        final ChessMan chessMan = board[yPosFrom][xPosFrom].getChessMan();
        chessMan.setMoved(true);
        board[yPosFrom][xPosFrom].removeChessMan();
        if (!board[yPosTo][xPosTo].isEmpty()) {
            if (currentPlayer == Color.WHITE) {
                blackKilled.add(board[yPosTo][xPosTo].getChessMan());
            } else {
                whiteKilled.add(board[yPosTo][xPosTo].getChessMan());
            }
            board[yPosTo][xPosTo].removeChessMan();
        }
        board[yPosTo][xPosTo].addChessMan(chessMan);
        changePlayer();
        support.firePropertyChange(BORD_CHANGED, null, turn);
    }

    /**
     * Changes the current Player.
     */
    private void changePlayer() {
        currentPlayer = currentPlayer == Color.BLACK ? Color.WHITE : Color.BLACK;
    }
}
