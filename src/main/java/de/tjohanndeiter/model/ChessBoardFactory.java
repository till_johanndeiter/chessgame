package de.tjohanndeiter.model;

import de.tjohanndeiter.model.chessmen.Color;
import de.tjohanndeiter.model.moveOptions.OptionCalculator;
import de.tjohanndeiter.model.moveOptions.StandardOptionCalculator;


public class ChessBoardFactory {

    //Default Values
    private Color color = Color.WHITE;
    private OptionCalculator optionCalculator = new StandardOptionCalculator();
    private boolean multiplayer;
    private Color colorOfOpponent = Color.BLACK;

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        if (color == Color.WHITE) {
            colorOfOpponent = Color.BLACK;
        } else {
            colorOfOpponent = Color.WHITE;
        }
        this.color = color;
    }

    public boolean isMultiplayer() {
        return multiplayer;
    }

    public void setMultiplayer(boolean multiplayer) {
        this.multiplayer = multiplayer;
    }

    public GameManger getGameManger() {
        if (multiplayer) {
            return new GameManger(optionCalculator);
        } else {
            return new GameManger(new StandardOptionCalculator(), colorOfOpponent);
        }
    }
}
