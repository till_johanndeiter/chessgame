package de.tjohanndeiter.view;


import de.tjohanndeiter.parser.ChessManFactory;
import de.tjohanndeiter.controller.MoveController;
import de.tjohanndeiter.model.chessmen.ChessMan;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Extends the {@link StackPane} for better use with the logic of a chessman. Contains the {@link #normalBackground}
 * for the Color if unmarked. And uses the {@link #setColor(Color)} for mark it with a another. {@link #imageView}
 * represents the imageView for the {@link ChessMan} form the model.
 * {@linkplain javafx.scene.control.Button} added in {@link ChessManFactory}
 * used for communication with {@link MoveController}
 * TODO: Better solve for the mark of the background. Create own mehthod for mark as reachable and attak.
 */

public class ChessSquareView extends StackPane {

    private static final Color ATTACK_COLOR = Color.RED;
    private static final Color LEGAL_COLOR = Color.ORANGE;
    private static final int GROUD_COLOR_POSTION = 0;
    private static final int IMAGE_POSITION = 2;

    private Color normalBackground;

    private ImageView imageView;

    private boolean marked;


    ChessSquareView(Color normalBackground) {
        super();
        this.normalBackground = normalBackground;
        this.setMaxSize(Region.USE_PREF_SIZE, Region.USE_PREF_SIZE);
        marked = false;
    }

    public void markAsAttack(){
        setColor(ATTACK_COLOR);
    }

    public void markAsLegal() {
        setColor(LEGAL_COLOR);
    }

    //TODO: REMOVE
    private void setColor(Color color) {
        Rectangle rectangle = (Rectangle) super.getChildren().get(GROUD_COLOR_POSTION);
        rectangle.setFill(color);
        marked = true;
    }

    /**
     * Removes the current imageView and add a new.
     *
     * @param imageView imageView of a chessman .png
     */
    public void changeImageView(ImageView imageView) {
        if (super.getChildren().size() > IMAGE_POSITION) {
            super.getChildren().remove(IMAGE_POSITION);
        }
        this.imageView = imageView;
        super.getChildren().add(IMAGE_POSITION, imageView);
    }

    /**
     * Removes the {@link #imageView}.
     */
    public void removeImageView() {

        if (super.getChildren().size() > IMAGE_POSITION) {
            super.getChildren().remove(IMAGE_POSITION);
        }
    }

    /**
     * Set the #color (back) to normal.
     */
    public void unmark() {
        marked = false;
        Rectangle rectangle = (Rectangle) super.getChildren().get(GROUD_COLOR_POSTION);
        rectangle.setFill(normalBackground);
    }

    /**
     * Set the #color to the selectColor if button is clicked. The action Event is set up in {@link BoardViewFactory}.
     */
    void clickedOn() {
        if (imageView != null) {
            Rectangle rectangle = (Rectangle) super.getChildren().get(GROUD_COLOR_POSTION);
            if (!marked) {
                rectangle.setFill(Color.BLUE);
                marked = true;
            } else {
                rectangle.setFill(normalBackground);
                marked = false;
            }
        }
    }

    public ImageView getImageView() {
        return imageView;
    }

    public boolean isMarked() {
        return marked;
    }
}
