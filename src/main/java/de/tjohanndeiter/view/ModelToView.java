package de.tjohanndeiter.view;

import de.tjohanndeiter.model.chessmen.ChessMan;
import javafx.scene.image.ImageView;

public interface ModelToView {
    ImageView modelChessMenToImgView(ChessMan chessMan);
}
