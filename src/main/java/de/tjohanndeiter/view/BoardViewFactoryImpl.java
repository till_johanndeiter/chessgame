package de.tjohanndeiter.view;

import de.tjohanndeiter.controller.MoveController;
import de.tjohanndeiter.model.GameManger;
import de.tjohanndeiter.model.Field;
import de.tjohanndeiter.model.chessmen.ChessMan;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Implements interface {@link BoardViewFactory} {@inheritDoc}.
 */
//TODO: Dynamic size of chess fields
public class BoardViewFactoryImpl implements BoardViewFactory {

    private static final int BOARD_SIZE = 8;
    private static final int GROUD_COLOR_POSTION = 0;
    private static final int BUTTON_POSTION = 1;

    private static final Color BRIGHT_COLOR = Color.WHEAT;
    private static final Color DARKER_COLOR = Color.BLANCHEDALMOND;
    private static final Color MOVED_OVER_COLOR = Color.GREEN;

    private ModelToView modelToView = new StandardModelToView();

    /**
     * Adds to the #viewBoard and the #chessSquareViews the icons for the
     * {@link ChessMan} with the {@link ModelToView} and create a {@link Button}
     * on each {@link ChessSquareView} with a connection to {@link MoveController} to perform a Move
     *
     * @param chessBoard     chessBoard from the {@see model}
     * @param viewBoard      gridPane to set up
     * @param moveController controller for making a move
     * @return Array of {@link ChessSquareView} for better control compared to a {@link GridPane}
     */
    @Override
    public BoardView createChessSquares(GameManger chessBoard, GridPane viewBoard,
                                        MoveController moveController) {
        Field[][] chessSquares = chessBoard.getBoard();
        ChessSquareView[][] chessSquareViews = new ChessSquareView[BOARD_SIZE][BOARD_SIZE];
        for (int j = 0; j < chessSquares.length; j++) {
            for (int i = 0; i < chessSquares.length; i++) {
                ImageView imageView = null;
                if (!chessSquares[j][i].isEmpty()) {
                    imageView = modelToView.modelChessMenToImgView(chessSquares[j][i].getChessMan());
                }
                Color color;
                if (i % 2 == 0 && j % 2 == 0 || i % 2 != 0 && j % 2 != 0) {
                    color = BRIGHT_COLOR;
                } else {
                    color = DARKER_COLOR;
                }

                ChessSquareView chessSquareView = new ChessSquareView(color);
                addBackground(color, chessSquareView);
                setUpActionButton(j, i, moveController, chessSquareView);
                if (imageView != null) {

                    imageView.setFitHeight(40);
                    imageView.setFitWidth(40);
                    imageView.setMouseTransparent(true);
                    chessSquareView.changeImageView(imageView);
                }
                chessSquareView.setMaxHeight(1);
                chessSquareView.setMaxWidth(30);
                chessSquareViews[j][i] = chessSquareView;
                viewBoard.add(chessSquareView, i, j);
            }
        }
        return new BoardView(chessSquareViews);
    }

    /**
     * Helpmethod for {@link #createChessSquares(GameManger, GridPane, MoveController)} create a rectangle and set the
     * correct Standard {@link Color} to it.
     * TODO: find a solution for the move over color
     *
     * @param normalBackground Background in case if not marked
     * @param chessSquareView  chessSqareView which is created in
     *                         {@link #createChessSquares(GameManger, GridPane, MoveController)}
     */
    private void addBackground(Color normalBackground, ChessSquareView chessSquareView) {
        Rectangle rectangle = new Rectangle();
        rectangle.setFill(normalBackground);
        rectangle.setHeight(66);
        rectangle.setWidth(66);
        chessSquareView.getChildren().add(GROUD_COLOR_POSTION, rectangle);
    }

    /**
     * Help Method for {@link #createChessSquares(GameManger, GridPane, MoveController)}. Created a Button an set it
     * on the {@link ChessSquareView} at position #yPos #xPos. The action event transfer the current postion to the
     * {@link MoveController} for perform a move or mark a figure.
     *
     * @param yPos
     * @param xPos
     * @param moveController
     * @param chessSquareView
     */
    private void setUpActionButton(int yPos, int xPos, MoveController moveController, ChessSquareView chessSquareView) {
        Button button = new Button();
        button.setMinHeight(55);
        button.setMinWidth(55);
        button.setStyle("-fx-background-color: transparent;");
        button.setOnAction(event -> {
            chessSquareView.clickedOn();
            moveController.clickOnField(yPos, xPos);
        });
        chessSquareView.getChildren().add(BUTTON_POSTION, button);
    }
}
