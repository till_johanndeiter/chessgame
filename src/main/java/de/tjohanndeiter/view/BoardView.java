package de.tjohanndeiter.view;

import de.tjohanndeiter.model.MoveAbility;
import de.tjohanndeiter.utils.Quadruple;
import de.tjohanndeiter.utils.Tuple;
import javafx.scene.image.ImageView;

public class BoardView {

    private static final int BOARD_SIZE = 8;

    private ChessSquareView[][] boardView;

    public BoardView(final ChessSquareView[][] boardView) {
        this.boardView = boardView;
    }

    public void move(final Quadruple quadruple) {
        final int yPosFrom = quadruple.getYPosFrom();
        final int xPosFrom = quadruple.getXPosFrom();
        final int yPosTo = quadruple.getYPosTo();
        final int xPosTo = quadruple.getXPosTo();
        final ImageView temp = boardView[yPosFrom][xPosFrom].getImageView();
        boardView[yPosFrom][xPosFrom].removeImageView();
        boardView[yPosTo][xPosTo].changeImageView(temp);
        toNormal();
    }


    public void toNormal() {
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                boardView[i][j].unmark();
            }
        }
    }

    public void toNormal(final Tuple tuple) {
        final int notAtyPos = tuple.getYPos();
        final int notAtxPos = tuple.getXPos();
        for (Integer i = 0; i < BOARD_SIZE; i++) {
            for (Integer j = 0; j < BOARD_SIZE; j++) {
                if (!(i.equals(notAtyPos) && j.equals(notAtxPos))) {
                    boardView[i][j].unmark();
                }
            }
        }
    }

    public void markMovePossibillites(MoveAbility[][] moveAbilities) {
        for (int i = 0; i < boardView.length; i++) {
            for (int j = 0; j < boardView.length; j++) {
                switch (moveAbilities[i][j]) {
                    case ATTACK:
                        boardView[i][j].markAsAttack();
                        break;
                    case LEGAL:
                        boardView[i][j].markAsLegal();
                        break;
                    case ILLEGAL:
                        break;
                    default:
                        throw new IllegalArgumentException();
                }
            }
        }
    }
}
