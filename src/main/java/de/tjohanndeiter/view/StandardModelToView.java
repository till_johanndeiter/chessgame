package de.tjohanndeiter.view;


import de.tjohanndeiter.model.chessmen.ChessMan;
import de.tjohanndeiter.model.chessmen.Color;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.IOException;
import java.io.InputStream;


public class StandardModelToView implements ModelToView {

    @Override
    public ImageView modelChessMenToImgView(ChessMan chessMan) {
        String path = "/";
        switch (chessMan.getPiece()) {
            case QUEEN:
                path = path.concat("queen");
                break;
            case KING:
                path = path.concat("king");
                break;
            case BISHOP:
                path = path.concat("bishop");
                break;
            case KNIGHT:
                path = path.concat("knight");
                break;
            case ROOK:
                path = path.concat("rook");
                break;
            case PAWN:
                path = path.concat("pawn");
                break;
        }

        if (chessMan.getColor() == Color.BLACK) {
            path = path.concat("_black.png");
        } else {
            path = path.concat("_white.png");
        }
        Image image = null;
        try {
            InputStream inputStream = getClass().getResourceAsStream(path);
            image = new Image(inputStream);
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ImageView(image);
    }
}
