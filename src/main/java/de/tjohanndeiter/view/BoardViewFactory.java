package de.tjohanndeiter.view;

import de.tjohanndeiter.controller.MoveController;
import de.tjohanndeiter.model.GameManger;
import javafx.scene.layout.GridPane;

/**
 * Interface for create a clickable Grid.
 */
public interface BoardViewFactory {
    BoardView createChessSquares(GameManger chessBoard, GridPane viewBoard, MoveController moveController);
}
