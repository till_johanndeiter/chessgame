package de.tjohanndeiter;

import de.tjohanndeiter.model.moveOptions.StandardOptionCalculator;
import de.tjohanndeiter.model.moveOptions.OptionCalculator;
import de.tjohanndeiter.parser.Parser;

import java.io.File;

/**
 * Responsible for start the Application. Dependent on the args from the CommandLine it uses the {@link Mode}
 * for start the game in {@link GuiMode} oder {@link CommandLineMode}
 */

public final class ChessGame {
    /**
     * @param args Contains information's about the start mode and the start chessboard in a chessnotation implemented
     *             in a {@link Parser}
     */
    public static void main(final String... args) {

        Mode mode;
        String startFen;

        if (new File("saveFiles").mkdirs()) {
            System.out.println("saveFile Folder created");
        }
        final OptionCalculator optionCalculator = new StandardOptionCalculator();
        //Checks if the first param is equal to GUI
        //TODO start fen for the gui mode
        if (args.length > 0 && args[0].equals("--gui")) {
            mode = new GuiMode();
        } else {
            mode = new CommandLineMode();
        }
        try {
            startFen = args[1];
        } catch (ArrayIndexOutOfBoundsException e) {
            startFen = null;
        }
        mode.startGame(startFen);

    }
}
