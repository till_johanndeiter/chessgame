package de.tjohanndeiter.controller;

import de.tjohanndeiter.exception.parse.ParseException;
import de.tjohanndeiter.external.Load;
import de.tjohanndeiter.external.Save;
import de.tjohanndeiter.model.GameManger;
import de.tjohanndeiter.model.moveOptions.MoveAbbilities;
import de.tjohanndeiter.utils.Quadruple;
import de.tjohanndeiter.utils.Tuple;
import de.tjohanndeiter.view.BoardView;
import de.tjohanndeiter.view.BoardViewFactory;
import de.tjohanndeiter.view.BoardViewFactoryImpl;
import javafx.fxml.FXML;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.Setter;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;

@Getter
@Setter
public class MoveController implements PropertyChangeListener {


    @FXML
    private GridPane viewBoard;

    private BoardViewFactory boardViewFactory = new BoardViewFactoryImpl();

    private BoardView boardView;
    private ClickHandler clickHandler;
    private Save save;
    private Load load;


    @Override
    public void propertyChange(final PropertyChangeEvent event) {
        switch (event.getPropertyName()) {
            case ClickHandler.FIELD_CLICKED:
                boardView.toNormal((Tuple) event.getNewValue());
                break;
            case GameManger.BORD_CHANGED:
                boardView.move((Quadruple) event.getNewValue());
                break;
            case ClickHandler.OPTION_CALCULATED:
                boardView.markMovePossibillites(((MoveAbbilities) event.getNewValue()).getMoveAbilities());
                break;
            case ClickHandler.RESET:
                boardView.toNormal();
                break;

        }


    }

    public void init(final GameManger gameManger, final ClickHandler clickHandler, final Save save, final Load load) {
        this.boardView = boardViewFactory.createChessSquares(gameManger, viewBoard, this);
        this.clickHandler = clickHandler;
        this.save = save;
        this.load = load;
    }

    public void clickOnField(final Integer yPos, final Integer xPos) {
        clickHandler.handleClick(yPos, xPos);
    }


    @FXML
    public void gameRestart() {

    }

    @FXML
    public void saveGame() {
        FileChooser fileChooser = initFileChooser();
        File file = fileChooser.showSaveDialog(new Stage());
        try {
            save.createSaveFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void loadGame() {
        FileChooser fileChooser = initFileChooser();
        File file = fileChooser.showOpenDialog(new Stage());
        try {
            load.loadItemsToChessBoard(file);
            //TODO: problem with load
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }


    private FileChooser initFileChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save Game");
        fileChooser.setInitialFileName(".chessSave");
        fileChooser.setSelectedExtensionFilter(
                new FileChooser.ExtensionFilter("saveFiles", "*.chessSave"));
        return fileChooser;
    }
}
