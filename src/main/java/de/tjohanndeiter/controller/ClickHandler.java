package de.tjohanndeiter.controller;

import de.tjohanndeiter.exception.turn.TurnException;
import de.tjohanndeiter.model.GameManger;
import de.tjohanndeiter.model.MoveAbility;
import de.tjohanndeiter.model.chessmen.ChessMan;
import de.tjohanndeiter.model.moveOptions.MoveAbbilities;
import de.tjohanndeiter.model.moveOptions.OptionCalculator;
import de.tjohanndeiter.model.moveOptions.StandardOptionCalculator;
import de.tjohanndeiter.utils.Quadruple;
import de.tjohanndeiter.utils.Tuple;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class ClickHandler {

    public static final String FIELD_CLICKED = "FieldClicked";
    public static final String OPTION_CALCULATED = "OptionCalculated";
    public static final String RESET = "Reset";

    private final GameManger gameManger;
    private final Quadruple quadruple = new Quadruple();
    private final PropertyChangeSupport support = new PropertyChangeSupport(this);
    private final OptionCalculator optionCalculator = new StandardOptionCalculator();

    public ClickHandler(final GameManger gameManger) {
        this.gameManger = gameManger;
    }

    public void handleClick(final int yPos, final int xPos) {

        final ChessMan clickedMan = gameManger.getChessMan(yPos, xPos);
        if (isOwnMan(clickedMan) && isNewSelect(yPos, xPos)) {
            quadruple.reset();
            setFrom(yPos, xPos);
            MoveAbility[][] moveAbilities = optionCalculator.getMoveOptions(gameManger.getBoard(), yPos, xPos);
            support.firePropertyChange(FIELD_CLICKED, null, new Tuple(xPos, yPos));
            support.firePropertyChange(OPTION_CALCULATED, null, new MoveAbbilities(moveAbilities));
        } else if (isTarget(clickedMan)) {
            setTo(yPos, xPos);
            try {
                gameManger.tryMove(quadruple.getYPosFrom(), quadruple.getXPosFrom(),
                        quadruple.getYPosTo(), quadruple.getXPosTo());
            } catch (TurnException e) {
                reset();
            }
        } else {
            reset();
        }

    }

    private void reset() {
        quadruple.reset();
        support.firePropertyChange(RESET, null, null);
    }

    private void setTo(final int yPos, final int xPos) {
        quadruple.setYPosTo(yPos);
        quadruple.setXPosTo(xPos);
    }

    private void setFrom(final int yPos, final int xPos) {
        quadruple.setYPosFrom(yPos);
        quadruple.setXPosFrom(xPos);
    }

    private boolean isNewSelect(final int yPos, final int xPos) {
        return !quadruple.halfSetted() || (quadruple.getYPosFrom() != yPos || quadruple.getXPosFrom() != xPos);
    }

    private boolean isTarget(final ChessMan clickedMan) {
        return quadruple.halfSetted() && (!isOwnMan(clickedMan));
    }

    private boolean isOwnMan(final ChessMan clickedMan) {
        if (clickedMan == null) {
            return false;
        } else {
            return clickedMan.getColor() == gameManger.getCurrentPlayer();
        }

    }

    public void addObserver(final PropertyChangeListener observer) {
        support.addPropertyChangeListener(observer);
    }
}
