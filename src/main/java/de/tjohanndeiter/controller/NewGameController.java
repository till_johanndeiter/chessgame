package de.tjohanndeiter.controller;

import de.tjohanndeiter.external.Load;
import de.tjohanndeiter.external.Save;
import de.tjohanndeiter.model.chessmen.Color;
import de.tjohanndeiter.model.GameManger;
import de.tjohanndeiter.model.ChessBoardFactory;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;


public class NewGameController {

    private ChessBoardFactory chessBoardCreator = new ChessBoardFactory();
    private Color wantedColor;

    @FXML
    public void selectBlack(ActionEvent e) {
        chessBoardCreator.setColor(Color.BLACK);
    }

    @FXML
    public void selectWhite(ActionEvent e) {
        chessBoardCreator.setColor(Color.WHITE);
    }

    @FXML
    public void selectMultiplayer(ActionEvent e) {
        chessBoardCreator.setMultiplayer(true);
    }

    @FXML
    public void selectAi(ActionEvent e) {
        chessBoardCreator.setMultiplayer(false);
    }

    @FXML
    public void startGame(ActionEvent e) {
        final GameManger gameManger = chessBoardCreator.getGameManger();
        final ClickHandler clickHandler = new ClickHandler(gameManger);
        final Load load = new Load(gameManger);
        final Save save = new Save(gameManger);
        try {
            final FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("ChessBoardView.fxml"));
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            final MoveController moveController = fxmlLoader.getController();
            moveController.init(gameManger, clickHandler, save, load);
            clickHandler.addObserver(moveController);
            gameManger.addObserver(moveController);
            Stage window = (Stage) ((Node) e.getSource()).getScene().getWindow();
            window.setTitle("ChessGame");
            window.setScene(scene);
            window.show();

        } catch (IOException ioEx) {
            ioEx.printStackTrace();
        }
    }
}
