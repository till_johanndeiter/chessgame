package de.tjohanndeiter.controller;

import de.tjohanndeiter.external.Load;
import de.tjohanndeiter.external.Save;
import de.tjohanndeiter.model.GameManger;
import de.tjohanndeiter.model.moveOptions.StandardOptionCalculator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

public class MenuController {

    private static final String TITLE = "ChessGame";

    @FXML
    public void quickStart(ActionEvent event) {
        try {
            final FXMLLoader fxmlLoader = new FXMLLoader(getFXMl("ChessBoardView.fxml"));
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            final MoveController moveController = fxmlLoader.getController();
            final GameManger game = new GameManger(new StandardOptionCalculator());
            final ClickHandler clickHandler = new ClickHandler(game);
            final Save save = new Save(game);
            final Load load = new Load(game);
            moveController.init(game, clickHandler, save, load);
            clickHandler.addObserver(moveController);
            game.addObserver(moveController);
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setResizable(false);
            window.setTitle(TITLE);
            window.setScene(scene);
            window.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private URL getFXMl(final String s) {
        return getClass().getClassLoader().getResource(s);
    }

    @FXML
    public void normalPlay(ActionEvent event) {
        try {
            final FXMLLoader fxmlLoader = new FXMLLoader(getFXMl("NewGameMenu.fxml"));
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            final NewGameController newGameController = fxmlLoader.getController();
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setResizable(false);
            window.setTitle(TITLE);
            window.setScene(scene);
            window.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void toSettings(ActionEvent event) {

    }
}
