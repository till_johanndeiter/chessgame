package de.tjohanndeiter.utils;

import de.tjohanndeiter.model.chessmen.Piece;
import de.tjohanndeiter.model.chessmen.Color;
import de.tjohanndeiter.model.Field;
import de.tjohanndeiter.parser.CharCreator;
import de.tjohanndeiter.parser.fen.FenCharCreator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

//TODO write test and Doku
public class ValidationUtils {


    private static final int CHESSBOARD_SIZE = 8;
    private static final CharCreator CHAR_CREATOR = new FenCharCreator();


    public static boolean correctChessboard(Field[][] chessSquares) {

        HashMap<Character, Integer> numbersOfChessMans = new HashMap<>();

        for (int i = 0; i < CHESSBOARD_SIZE; i++) {
            for (int j = 0; j < CHESSBOARD_SIZE; j++) {
                if (!chessSquares[i][j].isEmpty()) {
                    Character chessManKey = CHAR_CREATOR.chessSquareToChar(chessSquares[i][j]);
                    if (numbersOfChessMans.containsKey(chessManKey)) {
                        int newValue = numbersOfChessMans.get(chessManKey) + 1;
                        numbersOfChessMans.put(chessManKey, newValue);

                    } else {
                        numbersOfChessMans.put(chessManKey, 1);
                    }

                }
            }
        }

        Set<Character> set = numbersOfChessMans.keySet();

        for (Character character : set) {
            int count = numbersOfChessMans.get(character);
            int maxCorrect = 2;
            if (Character.toLowerCase(character) == 'p') {
                maxCorrect = 8;
            } else if (Character.toLowerCase(character) == 'q' || Character.toLowerCase(character) == 'q') {
                maxCorrect = 1;
            }
            if (count > maxCorrect || count < 0) {
                return false;
            }
        }

        return validateBishop(chessSquares);
    }


    private static boolean validateBishop(Field[][] chessSquares) {

        List<Tuple> blackBishopPositions = new ArrayList<>();
        List<Tuple> whiteBishopPositions = new ArrayList<>();


        for (int i = 0; i < CHESSBOARD_SIZE; i++) {
            for (int j = 0; j < CHESSBOARD_SIZE; j++) {
                Field chessSquare = chessSquares[i][j];
                if (!chessSquare.isEmpty() && chessSquare.getChessMan().getPiece() == Piece.BISHOP) {
                    if (chessSquare.getChessMan().getColor() == Color.WHITE) {
                        whiteBishopPositions.add(new Tuple(i, j));
                    } else {
                        blackBishopPositions.add(new Tuple(i, j));
                    }
                }
            }
        }

        return validatePair(blackBishopPositions) && validatePair(whiteBishopPositions);
    }


    private static boolean validatePair(List<Tuple> tuples) {
        if (tuples.size() == 2) {
            Tuple first = tuples.get(0);
            Tuple second = tuples.get(1);

            if (whiteField(first) && whiteField(second)) {
                return false;
            }

        }

        return true;

    }


    private static boolean whiteField(Tuple tuple) {

        int yPos = tuple.getYPos();
        int xPos = tuple.getXPos();

        if (yPos % 2 != 0 && xPos % 2 == 0 || yPos % 2 == 0 && xPos % 2 != 0) {
            return false;
        } else {
            return true;
        }
    }
}
