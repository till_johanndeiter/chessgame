package de.tjohanndeiter.utils;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * Class for store the coordinates of the current square and the target square.
 */
@Getter
@Setter
@NoArgsConstructor
public class Quadruple {


    private Integer xPosFrom;
    private Integer yPosFrom;
    private Integer xPosTo;
    private Integer yPosTo;

    public Quadruple(int yPosFrom, int xPosFrom, int yPosTo, int xPosTo) {
        this.xPosFrom = xPosFrom;
        this.yPosFrom = yPosFrom;
        this.xPosTo = xPosTo;
        this.yPosTo = yPosTo;
    }

    public boolean halfSetted() {
        return (xPosFrom != null && yPosFrom != null);
    }

    public boolean isSetted() {
        return (xPosFrom != null && yPosFrom != null && xPosTo != null && yPosTo != null);
    }

    public void reset() {
        yPosFrom = null;
        yPosTo = null;
        xPosFrom = null;
        xPosTo = null;
    }
}
