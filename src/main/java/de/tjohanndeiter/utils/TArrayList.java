package de.tjohanndeiter.utils;

import java.util.ArrayList;

/**
 * ArraysList with a {{@link #get()}} Method which return the Element at the last idx.
 * @param <T> Type of the Elements in a List.
 */
public class TArrayList<T> extends ArrayList<T> {
    private static final long serialVersionUID = 8582433437601788991L;

    Object get() {
        return super.get(super.size() - 1);
    }
}
