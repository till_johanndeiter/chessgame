package de.tjohanndeiter.utils;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Tuple {
    private int xPos;
    private int yPos;
}
