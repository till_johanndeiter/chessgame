package de.tjohanndeiter.utils;

import de.tjohanndeiter.exception.parse.ParseFieldException;
import de.tjohanndeiter.model.Field;
import de.tjohanndeiter.parser.fen.FenParser;
import de.tjohanndeiter.parser.Parser;

public final class GeneratorUtils {


    public static Field[][] standardBegin() {
        Parser fenParser = new FenParser();
        Field[][] chessSquare = null;
        try {
            chessSquare = fenParser.stringToField("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w");
        } catch (ParseFieldException e) {
            e.printStackTrace();
        }

        return chessSquare;
    }
}
